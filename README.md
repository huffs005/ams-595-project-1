# User Guide #

In order to use this code you must have afunctioning version of MATLAB.

You will have to pull MC_PI1.m, MC_PI2.m, and MC_PI3.m.

TO RUN
MC_PI1: You can just run this script by pressing 'run' in MATLAB, no input or extra setup required.
MC_PI2: You can also just run this code by pressing 'run' in MATLAB. You should change the variable n to your desired level of precision for the estimate of pi
before you run this code though.
MC_PI3: This code will ask you for a desired level of precision with 0 specifying the first number after the decimal point. Then, you can just press 'run' in
MATLAB to run this code as well.

Note that MC_PI1 will produce a graph of the difference between the estimated value and the actul value of pi vs the number of points used. Similarly, 
MC_PI3 will create a plot in real time of the points being used as they are generated and will color the points inside the unit circle red and thos outside blue.